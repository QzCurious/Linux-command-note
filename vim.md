# Vim
- `K`: search manual of hovered word

## Normal Mode
### Cursor motions
```
     ^
     k
< h     l >
     j
     v
```
- `w`: to next beginning of a word
- `W`: to next beginning of a WORD(separated by space)
- `e`: to next end of a word (`ge` backword)
- `E`: to next end of a WORD(separated by space)
- `^`: Move cursor to first non-blank character of the line
- `[count]_`: Like `^`; `count-1` lines downword, while given a `count`
- `g_`: Move cursor to the last non-blank character of the line
- `0`: Move cursor to start of the line
- `$`: Move cursor to end of the line
- `H`: Jump to top of the screen
- `M`: Jump to middle of the screen
- `L`: Jump to bottom of the screen
- `gg`: Move cursor to start of the file
- `G`: Move cursor to bottom of the file
- [N]G: Move cursor to [N]th line
- `%`: Move cursor to paired {}, (), []
- `CTRL-O`: Jump to older position
- `CTRL-I`: Jump to newer position
- `whichwrap`: 

### Search & Substitute
#### search
- tip: `/{pattern}` `:s/{previous}/{new}` substitute with highlight
- `/{pattern}`, `?{pattern}`: (Move cursor) Search {pattern} forward/backward  
  `n`: next {pattern}  
  `N`: previous {pattern}  
  *{pattern}* is in regexp from  
  *\c* in *{pattern}* ignore the case
- `f/F{char}`: Foreword/Backword onto {char} (`;`/`,` for next/previous)
- `t/T{char}`: Foreword/Backword till  {char} (`;`/`,` for next/previous)
- `*`: Find word under cursor - forward  (bounded)
- `g*`: Find word under cursor - forward  (unbounded)
- `#`: Find word under cursor - backward (bounded)
- `g#`: Find word under cursor - backward (unbounded)
- `:set incsearch`: Highlight matches while typing; *note: +reltime*
#### substitute
- By default, `:s` command work on the first occurence of a line (range
- `:[range]s/{pattern}/{string}/[flags]`: `s`: substitute
- `:h range`, `:h s_flag`
- `:%s/{pattern}/{string}/g`: Substitute in whole file

##### [flag]
- `[g]`: Replace all occurrences in the line
- `[c]`: Confirm each replacement
- `[e]`: Avoid pattern fails


- `:s/old/new`: To substitute new for the first old in a line type
- `:s/old/new/g`: To substitute new for all 'old's on a line type
- `#,#s/old/new/g`: To substitute phrases between two line #'s type
- `:%s/old/new/g`: To substitute all occurrences in the file type
- `:%s/old/new/gc`: To ask for confirmation each time add 'c'

### Scroll
- `zt`: Cursor line at top of window
- `zz`: Cursor line at center of window
- `zb`: Cursor line at bottom of window
- `CTRL-D`/`CTRL-F`: Scroll window half/full page up
- `CTRL-U`/`CTRL-B`: Scroll window half/full page down
- `CTRL-E`: Scroll window one line up
- `CTRL-Y`: Scroll window one line down
- `zh`/`zl`: Scroll view [count] characher horizontally ('nowrap')
- `zH`/`zL`: Scroll view half a screenwidth horizontally ('nowrap')
- `zs`/`ze`: Scroll view to left/right of cursor ('nowrap')
- `'sidescroll'`: Amount of column of horizontal scrolling while reach to the edge of window


### Folding (save folding: :mkview)
- `zf{motion}`: Create a fold by motion
- `zo`/`zc`: (*cursor-wise*) O-pen a fold / C-lose a fold
- `zO`/`zC`: (*cursor-wise*) O-pen all nested folds / C-lose all nested folds
- `zr`/`zm`: (*window-wise*) R-educe(`zo`) fold (increase foldlevel) / Folds m-ore(`zc`) (decrease foldlevel)
- `zR`/`zM`: (*window-wise*) Unfold/fold all (nested) folds
- `zn`/`zN`/`zi`: (*window-wise*) Fold none / Fold all / Toggle between `zn` and `zN`
- `zd`/`zD`: (*cursor-wise*) Delete a/all fold(s)
- 'foldmethod': `manual` / `indent` / `syntax`
- 'foldcolumn': Indicating foldable region by `-`, `+` signs
- 'foldlevel': `:set foldlevel=0` close all folds
- 'foldopen' / 'foldclose': Set folding corresponding to cursor; `set foldopen=all foldclose=all`


### Edit file
- `.`: Repeat last change
- `d{motion}`: Delete text that `motion` moves over
- `dd`: Delete a whole line
- `D`: Delete character under cursor until the end of line
- `J`: Join lines (Delete the linebreak of the line under cursor)
- `y{motion}`: Yank text that `motion` moves over
- `[count]yy`: Yank a line
- `Y`: Same as `yy`
- `x`: Delete a character under cursor
- `X`: Delete a character before cursor
- `~`: Change case of a character (works on visual mode as well)
- `u`: Undo [count] changes
- `U`: Undo changes on a line. `U` itself also counts as a change, and thus `U` undoes a previous `U`
- `CTRL-R`: Undo the undo's
- `p`: Put to back (if a line is catched, put under the line)
- `P`: Put to front (if a line is catched, put above the line)
- `r[char]`: Replace a character under cursor
- `R`: Enter replace mode.

### Enter insert mode
- `i`: Enter insert mode
- `I`: Move cursor to first non-blank character and enter insert mode
- `gI`: Insert from start of the line (like `0i`)
- `s`: Delete character under cursor and enter insert mode
- `S`: Move cursor to beginning of the line and delete characters under cursor until end of the line
- `a`: Enter insert mode after cursor
- `A`: Enter insert mode at the end of line
- `o`: Make a new line below and enter insert mode at the new line
- `O`: Make a new line above and enter insert mode at the new line
- `c{motion}`: Delete {motion} text and enter insert mode
- `cc`: Change a whole line (enter insert mode)
- `C`: Perform `D` and enter insert mode

## v_Visual mode
- `v_o`: Move cursor to the other end
- `v_O`: Move cursor blockwise

### `v` visual mode

### `V` linewise visual mode

### `CTRL-V` blockwise visual mode
- `I`: Insert
- `A`: Append
- `v_$`: Highlight to (ecah) end of line
- `v_c`/`v_s`: Delete text in highlight area and enter insert mode
- `v_C`: Delete text till end of line and enteer insert mode
- `v_~`, `v_U`, `v_u`: Switch case; To Upper case; to lower case

### Switch window
- `CTRL-W_CTRL-W`: Switch to another window

## :Command-line Mode
- `<TAB>`: auto complete command
- `CTRL-D`: list posible commands 
- `:!command`: execute a command
- `CTRL-P`/`CTRL-N`: Iterate through command history




## "register (register is/isn't named by user)
- `:registers`: show all registers in used
- `"xyy`: Yank a whole line to register x

### named register
- by [a-z], re-write register. by [A-Z], append on register

### unnamed register
- `"0`: record lastly yanked text


## Insert Mode
- `CTRL-R`+`{register}`: paste content of `{register}` on. `yiw` yank a word to register0 then move to a word then `ciw`+`CTRL-R`+`0` replace the word by yanked word

## Marco
- `.`: Repeat last change
- `q{register}`: record marco to `{register}`, press q again to stop recoding
- `[N]@{register}`: perform instructions recorded in `{register}`
- `@@`: Execute the last performed macro

## Deal with files
- `$ vim file1 file2`: Edit multiple files
- `:quit[!]`: Quit
- `:edit {file}`: Put current file to buffer and edit the file named `{file}`
- `:hide edit {file}`: Edit `{file}` without saving current file
- `:write [fileName]`: `:w` save the file / Write current content to a file name `fileName`
- `:write ! {command}`: Write current content as input to `{command}`
- `:read {fileName}`: read `{fileName}` and put it below cursor
- `:read ! {command}`: read output of `{command}` and put it below cursor position
- `:[w]next[!]`/`:[w]previous[!]`: To next/previous arg; `w`: write and switch; `!`: discard changes (`bn`/`bp`)
- `CTRL-^`: Switch between previous one buffer
- `:args [arglist]`: Show argument list; Add args into vim (space separated)

### Filter
- A `{filter}` is a **program** that accepts text at standard input, changes it in some way, and sends it to standard output.
- `!{motion}{filter}`
- `!!{filter}: Linewise filtering
<table align="center">
  <tr> <th colspan="2">Example</th> </tr>
  <tr>
    <td colspan="2" align="center" > <code>!2jsort</code> </td>
  </tr>
  <tr align="center">
    <td>Before</td>
    <td>After</td>
  </tr>
  <tr align="left">
    <td>
      <pre>
      111  ← cursor here
      555
      222
      </pre>
    </td>
    <td>
      <pre>
      111
      222
      555
      </pre>
    </td>
  </tr>
</table>

## Buffers & Windows(`h: Q\_wi`) & Tabs(`h: 08.9`)
- `:[N]split [file]`/`CTRL-W`+`s`: Split window horizontally; `N`: make new window N high
- `:[N]vsplit [file]`/`CTRL-W`+`v`: Split window vertically; `N`: make new window N high
- `:on[ly]`/`CTRL-W`+`CTRL-O`: Leaves only current window
- `:close`/`CTRL-C`: close a window
- `:qall[!]`: Quit all windows; `!`: quit anyway
- `:w[q]all`: Write all; `q`: write and quit
- `:[v]new`: Split window `v`ertically/horizontally and open a new file
- `:ls`: Show all buffers (`:buffers`)
- `:bn`/`:bp`: To next/previous buffer (`:next`/`:previous`)
- `CTRL-W`+`CTRL-W`: Switch between windows
- `CTRL-W`+`h/j/k/l`: Move curser amoung windows
- `CTRL-W`+`H/J/K/L`: Move window to far-left/very-bottom/very-top/far-right

### Window size (`h: 08.3`)
- [`height`]`CTRL-W`+`\_`: Expand current window horizontally; `height`: make current window `height` high
- [`N`]`CTRL-W``+/-`: Resize window horizontally
- `'winheight'`, `'winminheight'`, `'winwidth'`, `'winminwidth'`, `'equalalways'`

### Tab
- `[N] tab {cmd}`
- `tabnext`/`gt`: Go to next tab
- `tabprevious`/`gT`: Go to previous tab


## Vim Help
- `CTRL-]`: Jump to definition of the keyword
- `CTRL-T`
- `CTRL-O`
- `CTRL-I`
- Show conceal symbols
```vimrc
:set conceallevel=0
:hi link HelpBar Normal
:hi link HelpStar Normal
```

## Mapping
- `:h keycodes`

## .vimrc
- [.vimrc example](http://vim.wikia.com/wiki/Example\_vimrc)
- `:help vimrc-intro`
- `:echo $MYVIMRC`: 


## 'option' (Variables) 
- `:options`: set options on the fly (`:h 05.7`)

### Setting options
- `:se[t]`: Show all options that differ from their default value.
- `:se[t] {option}?`: show value of option
- `:se[t] xxx`: Set the option `xxx`
- `:se[t] noxxx`: Prepend `no` to unset option `xxx`
- `:se[t] {option}&`: Set option to default
- `:se[t] wrap`: wrap exceed text by break it two to lines

### options
- [autoindent](http://vimdoc.sourceforge.net/htmldoc/options.html#'autoindent')
- [expandtab](http://vimdoc.sourceforge.net/htmldoc/options.html#'expandtab')
- [shiftwidth](http://vimdoc.sourceforge.net/htmldoc/options.html#'shiftwidth')
- [tabstop](http://vimdoc.sourceforge.net/htmldoc/options.html#'tabstop')
- [softtabstop](http://vimdoc.sourceforge.net/htmldoc/options.html#'softtabstop')
- ignorecase: ignore case while searching
- cursorline: underlining the line cursor is at
- number, relativenumber
- incsearch

### Accessablility
- [scriptname]

## filetype & plugin
- There two things relat to filetype: `plugin`, `indent`
- `filetype on`: Turn on filetype detection (it also used for syntax highlighting `syntax on`)
- `:filetype detect`: Do detection
- Adding a help file of a plugin: `:h 05.6`

## Indent
- `$VIMRUNTIME/indent`: Indentation files for different filetypes
- `:set autoindent`
- `==`/`={motion}`: Auto indent

## Marks
- `:marks [mark(s)]`: list all marks; list only the `mark(s)`
- `m{mark}`: make a mark
- ``{mark}`: move to `{mark}`. `g`{mark}` does the same without change jumplist
- `'{mark}`: move to first non-blink of line of {mark} (like ``{mark}` `^`). `g'{mark}` does the same without change jumplist
- <code>\`\`</code> / `''`: move back to **latest** position

## Highlight
`:set hls`: Highlight matches (highlight search )
`:nohls`: Disable highlight matches temporary

## Mouse
- Enable mouse in tmux (support windows resizing)
  ```
  https://unix.stackexchange.com/questions/50733/cant-use-mouse-properly-when-running-vim-in-tmux
  vimrc:
    :set ttymouse=xterm2
    :set mouse=a
  tmux.conf:
    set -g mouse on
  ```
- `'mouse'`

## miscellaneous
### backup `:h 07.4`
### change case
- `~`: change case of a character
- `g~{motion}`: change case through `{motion}`; `g~~` for whole line
- `gU{motion}`, `gu{motion}`: change case to upper/lower through {motion}; `gUU`, `guu`: for whole line
#### colorscheme
- `colorscheme {nameOfColorScheme}`
- colorschemes are store in `$VIMRUNTIME/colors` or `~/.vim/colors`

### showing invisible character, tabs('^I'), end of line('$')
- `:set list`: Turn on `'lsit'`, try `:list`
- `'listchars`: Change the appearance of `:list`
- `set listchars=tab:▸\ ,eol:↩,trail:∙,extends:»,precedes:«`

### global command
- `:g[!]/{pattern}/[cmd]: Execute `cmd` (default `:p`) on the lines where `{pattern}` matches; `!` on where `{pattern}` not match

### Comparing files
- `:diffsplit {filename}`

## Regular Expression
- `\<`: Start of a word
- `\>`: End of a word
- `\( \)`: Grouping

## Tips
- `xp`: useful for miss spelling. (Place a character to right)
#### convert tabs and spaces
```vimrc
" tabs to spaces
:set expandtab
:set retab!
```
```vimrc
" spaces to tabs
:set noexpandtab
:set retab!
```
- `:retab!`: Replace all tabs to current value of `'tabstop'`. Use `!` for converting all.




### References
- [vimdoc](http://vimdoc.sourceforge.net/htmldoc/)
- [vimdoc Notation](http://vimdoc.sourceforge.net/htmldoc/intro.html#notation)
- [Vim Training Class 1 - Basic motions and commands](https://www.youtube.com/watch?v=Nim4_f5QUxA&t=581s)

## In termianl
- `vim -N -u NONE`: Plain vim
- `vim -R {file}`: Readonly; `:w!`: force to write no matter it's readonly
- `vim -M {file}`: Forbid any changes; `:set modifiable``:set write` to remove the protection
- `vim [-o/-O] {file1, file2,...}`: Open multiple files; `-o/-O`: open file to windows
- `vimdiff {file1} {file2}`: Compare two files (`:h 08.7`)


## TODO
- snippets, autocomplete, abbreviation
- make, copen
- clipboard
