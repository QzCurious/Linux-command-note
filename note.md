# Commands
## What is The Shell
- `date`
- `cal`
- `df`: (abbreviation for disk free)
- `free`: Display amount of free memory

## Navigation
- `pwd`
- `cd`: `.`, `..`, `-`, `~`
- `ls`: `-a`, `-l`, `-t`(by time) `--reverse`

## Exploring The System
- `ls {dir1} {dir2}...`
- `command -options args`
- `ls -al`: Show options can be strung

`ls` option | Long format | Option Description
:---:|:---:|---
`-a` | `--all` | List all files, even those with names that begin with a period, which are normally not listed (i.e., hidden).
`-A` | `--almost-all` | List all files, even those with names that begin with a period, which are normally not listed (i.e., hidden).
`-d` | `--directory` | Ordinarily, if a directory is specified, ls will list the contents of the directory, not the directory itself. Use this option in conjunction with the `-l` option to see details about the directory rather than its contents.
`-F` | `--classify` | This option will append an indicator character to the end of each listed name. For example, a “/” if the name is a directory.
`-h` | `--human`-readable | In long format listings, display file sizes in human readable format rather than in bytes.
`-l` | | Display results in long format.
`-r` | `--reverse` | Display the results in reverse order. Normally, ls displays its results in ascending alphabetical order.
`-S` | | Sort results by file size.
`-t` | | Sort by modification time.

filetype & access rights | # hard links | owner | group | size in byte | modification time | file name
-----------|---|----|----|---------|--------------|--------------------------
-rw-rw-r-- | 1 | qz | qz | 2799403 | Mar  6 12:28 | The Linux Command Line.pdf

- `file {file}`
- `less {file}`
- symbolic link & hard link

## Manipulating Files And Directories
- `cp`, `mv`, `mkdir`, `rm`, `ln`
`mkdir <dir1> [dir2 [dir3...]]`

`cp` options | Long format | Meaning
:---:|:---:|---
`-a` | `--archive` | Copy the files and directories and all of their attributes, including ownerships and permissions. Normally, copies take on the default attributes of the user performing the copy.
`-i` | `--interactive` | Before overwriting an existing file, prompt the user for confirmation. If this option is not specified, cp will silently overwrite files.
`-r` | `--recursive` | Recursively copy directories and their contents. This option (or the `-a` option) is required when copying directories.
`-u` | `--update` | When copying files from one directory to another, only copy files that either don't exist, or are newer than the existing corresponding files, in the destination directory. This is useful when copying large numbers of file as it skips over files that don't need to be copied.
`-v` | `--verbose` | Display informative messages as the copy is performed.

`mv` option | Long format | Meaning
:---:|:---:|---
`-i` | `--interactive` | Before overwriting an existing file, prompt the user for confirmation. If this option is not specified, `mv` will silently overwrite files.
`-u` | `--update` | When moving files from one directory to another, only move files that either don't exist, or are newer than the existing corresponding files in the destination directory.
`-v` | `--verbose` | Display informative messages as the move is performed.

`rm` option | Long format | Meaning
:---:|:---:|---
`-i` | `--interactive` | Before overwriting an existing file, prompt the user for confirmation. If this option is not specified, `mv` will silently overwrite files.
`-r` | `--recursive` | Recursively delete directories. This means that if a directory being deleted has subdirectories, delete them too. To delete a directory, this option must be specified.
`-f` | `--force` | Ignore nonexistent files and do not prompt. This overrides the `--interactive` option.
`-v` | `--verbose` | Display informative messages as the move is performed.

### Wildcards
Wildcards | Meaning
--- | ---
`*` | Matches any characters
`?` | Matches any single character
`[characters]` | Matches any character that is a member of the set characters
`[!characters]` | Matches any character that is not a member of the set characters
`[[:class:]]` | Matches any character that is a member of the specified class

#### Character Classes (`[[:class:]]`)
Character Classes | Meaning
--- | ---
`[:alnum:]` | Matches any alphanumeric character
`[:alpha:]` | Matches any alphabetic character
`[:digit:]` | Matches any numeral
`[:lower:]` | Matches any lowercase letter
`[:upper:]` | Matches any uppercase letter

- Hard link: `ln file link`
- Symbolic link: `ln -s file link`

- `ls -i`: List with inode number
- Set relative pathnames for symbolic links is a useful

## Working With Commands

- A command can be: **executable program**, **shell builtin command**, **shell function(script)**, **alias**
- `type`, `which`, `help (--help)`, `man`, `apropos`, `info`, `whatis`, `alias`
- `man <section#> <command>`

Section | Contents
:---:|---
1 | User commands
2 | Programming interfaces for kernel system calls
3 | Programming interfaces to the C library
4 | Special files such as device nodes and drivers
5 | File formats
6 | Games and amusements such as screen savers
7 | Miscellaneous
8 | System administration commands

### README And Other Program Documentation Files
- Many software packages installed on your system have documentation files residing in the `/usr/share/doc` directory.
- `zless`

### Creating Your Own Commands With alias
- `;` separate commands
- `alias name='string'`; `name` is the name of alias command, `string` refer to the behavior of `name`
- `unalias aliasname`
- Try `type aliasname` before do `alias` to prevent from lay over an existing command

## Redirection
- `cat`, `sort`, `uniq`, `grep`, `wc`, `head`, `tail`, `tee`
- stdin, stdout, stderr
- Redirect stdout: `ls -l > file`
- Append redirection: `ls -l >> file`
- File descriptor: 0-stdin, 1-stdout, 2-stderr
- Redirect stdout as well as stderr: `ls -l > file 2>&1` or `ls -l &> file`
- Redirect stdin: `ls -l < file` (the content of file is redirected to stdin, not "file" or filename itself)
- NOTICE, `>` and `>>` redirect content to a file, not a program.
- `ls -al > less` redirect the output to a file named less, but not `less` command.
- Pipeline: `command1 | command2`: pipe stdout of `command1` to stdin of `command2`
- `unqi`: Filter adjacent matching lines from INPUT (or standard input), writing to OUTPUT (or standard output)
- `unqi -d`: Show duplicate lines only
- `wc [-l]`: Print line, word, byte counts; `-l`: just print lines count
- `grep [-i|-v] pattern [file]`: Print lines containt `pattern`; `-i`: case insensitive; `-v`: inverse
- `head`, `tail`: `-n` to specify number of lines of output
- `tail -f file`: Monitor `file` in real-time
- `tee file...`: Read from stdin and write to stdout and files
- `ls -l | tee file | grep word`

## Seeing The World As The Shell Sees it
- `echo`

#### Expansion
#### Pathname Expansion
- The mechanism by which wildcards work is called *pathname expansion*
- `echo *` does not reveal hidden files
- `echo .*` can reveal hidden files

#### Tide Expansion
- `echo ~`: Print out current user home directory
- `echo ~user`: Print out user's home directory

#### Arithmetic Expansion
- `$((expression))`

Operator | Description
---|---
\+ | Addition
\- | Subtraction
\* | Multiplication
/ | Division (but remember, since expansion only supports integer arithmetic, results are integers).
% | Modulo, which simply means, “ remainder.”
\*\* | Exponentiation

- `echo $((9*(2+2)))` `()` can be used to reorder the computation order

#### Brace Expansion
- `echo Front-{A,B,C}-Back`: Front-A-Back Front-B-Back Front-C-Back
- `echo {01..15}`: 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15
- Zero-padded: `echo {01..15}`
- Reverse order: `echo {Z..A}
- Nested: `echo a{A{1,2},B{3,4}}b`: aA1b aA2b aB3b aB4b

#### Parameter Expansion
- `echo $USER`: USER is a variable in shell; print out current user name
- `env`\`printenv`
- `echo $nonexist`: Print out empty string

#### Command Substitution
- Output of a command as an expansion
- `ls -l $(which cp)`: `-rwxr-xr-x 1 root root 151024 Mar  3  2017 /bin/cp`

### Quoting
#### Double Quotes
- If we place text inside double quotes, all the special characters used by the shell lose their special meaning and are treated as ordinary characters. The exceptions are "$", "\\"(backslach), and "\`"(backquote). This means that ~~word-splitting~~, ~~pathname expansion~~, ~~tilde expansion~~, and ~~brace expansion~~ are suppressed, but parameter expansion, arithmetic expansion, and command substitution are still carried out.

> TODO
#### Single Quotes
#### Escaping Characters


## Advanced Keyboard Tricks
- `clear`, `history`

### Command Line Editing

#### Cursor Movement
Movement | Action
:---: | :---
`Ctrl-a` | Move cursor to the beginning of the line.
`Ctrl-e` | Move cursor to the end of the line.
`Ctrl-f` | Move cursor forward one character; same as the right arrow key.
`Ctrl-b` | Move cursor backward one character; same as the left arrow key.
`Alt-f` | Move cursor forward one word.
`Alt-b` | Move cursor backward one word.
`Ctrl-l` | Clear the screen and move the cursor to the top left corner. The clear command does the same thing.

#### Modifying Text
Modifying | Action
:---: | :---
`Ctrl-d` | Delete the character at the cursor location
`Ctrl-t` | Transpose (exchange) the character at the cursor location with the one preceding it.
`Alt-t` | Transpose the word at the cursor location with the one preceding it.
`Alt-l` | Convert the characters from the cursor location to the end of the word to lowercase.
`Alt-u` | Convert the characters from the cursor location to the end of the word to uppercase.

#### Cutting And Pasting (Killing And Yanking) Text
Copy & Paste | Action
:---: | :---
`Ctrl-k` | Kill text from the cursor location to the end of line.
`Ctrl-u` | Kill text from the cursor location to the beginning of the line.
`Alt-d` | Kill text from the cursor location to the end of the current word.
`Alt-Backspace` | Kill text from the cursor location to the beginning of the current Backspace word. If the cursor is at the beginning of a word, kill the previous word.
`Ctrl-y` | Yank text from the kill-ring and insert it at the cursor location.

### Completion
- `tab`: Auto completion
- `Alt-*`: Insert all possible completion
- More shell shortcut keys: `man READLINE`

## Using History
- `history`: Last 500 commands, store in `~/.bash_history`
- `!<number>`: History expansion

### Searching History
History Shortcut | Action
:---: | :---
Ctrl-p | Move to the previous history entry. Same action as the up arrow.
Ctrl-n | Move to the next history entry. Same action as the down arrow.
Alt-< | Move to the beginning (top) of the history list.
Alt-> | Move to the end (bottom) of the history list, i.e., the current command line.
Ctrl-r | Reverse incremental search. Searches incrementally from the current command line up the history list.
Alt-p | Reverse search, non-incremental. With this key, type in the search string and press enter before the search is performed.
Alt-n | Forward search, non-incremental.
Ctrl-o | Execute the current item in the history list and advance to the next one. This is handy if you are trying to re-execute a sequence of commands in the history list.

### History Expansion
History Expansion | Action
:---: | :---
`!!` | Repeat the last command. It is probably easier to press up arrow and enter.
`!<number>` | Repeat history list item number.
`!<string>` | Repeat last history list item starting with string.
`!?<string>` | Repeat last history list item containing string.

- `man HISTORY`: History Expansion

## Permissions
- `id`, `chmod`, `umask`, `su`, `sudo`, `chown`, `chgrp`, `passwd`

### Owners, Group Members, And Everybody Else
- In the Unix security model, a user may *own* files and directories, When a user owns a file or directory, the user has control over its access. Users cna, in turn, belong to a *group* consisting of one or more users who are given access to files and directories by their owners. In addition to granting access to a group, an owner may also grant some set of access rights to everybody, which in Unix terms is referred to as the *world*.
- User accounts are defined in the `/etc/passwd` file and groups are defined in the `/etc/group` file. When user accounts and groups are created, these files are modified along with `/etc/shadow` which holds information about the user's password. For each user account, the `/etc/passwd` file defines the user (login) name, uid, gid,the account's real name, home directory, and login shell.

File Type | Owner access rights | Group access rights | World access rights | ...
:---:|:---:|:---:|:---:|---
d | rwx | r-x | r-x | 2 qz qz 4096 Nov 15 11:30 Music//


File Type | Description
:---:|---
`-` | A regular file.
`d` | A directory.
`l` | A symbolic link. Notice that with symbolic links, the remaining file attributes are always “rwxrwxrwx” and are dummy values. The real file attributes are those of the file the symbolic link points to.
`c` | A character special file. This file type refers to a device that handles data as a stream of bytes, such as a terminal or modem.
`b` | A block special file. This file type refers to a device that handles data in blocks, such as a hard drive or CD-ROM drive.

Permission Attribute | Files | Directories
:---:|---|---
`r` | Allows a file to be opened and read. | Allows a directory's contents to be listed if the execute attribute is also set.
`w` | Allows a file to be written to or truncated, however this attribute does not allow files to be renamed or deleted. The ability to delete or rename files is determined by directory attributes. | Allows files within a directory to be created, deleted, and renamed if the execute attribute is also set.
`x` | Allows a file to be treated as a program and executed. Program files written in scripting languages must also be set as readable to be executed. | Allows a directory to be entered, e.g., cd directory.

#### chmod - Change File Mode
- Be aware that only the file's owner or the superuser can change the mode of a file or directory
- `chmod` supports **octal number representation** and **symbolic representation**

Symbolic Representation | Meaning
:---:|---
`u` | Short for **user** but means the file or directory owner
`g` | Group owner
`o` | Short for **others**, but means world
`a` | Short for **all**. The combination of `u`, `g`, and `o`

- `chmod 755 filename`
- Check out examples in *Table 9-6* in Textbook

#### umask - Set Default Permissions
- `umask 0022`: Unset group write permission and unset world write permission while new file is created.
- Meaning of first octet is described at P.99 in Textbook

### Changing Identities
1. Log out and log back in as the alternate user
2. Use the `su` command
3. Use the `sudo` command

- The `sudo` command allow an administrator to set up a configuration file called `/etc/sudoers`, and define specific commands that particular users are permitted to execute under an assumed identity
- `su [-[l]] [user]`: `l` for login; default user is superuser
- `su [user] -c '<command>'`: Execute `command` with `user` identity

`su` | `sudo`
---|---
Require corresponding user's password | Require current user's password
Enter a new shell with corresponding identity and environment | Stay in the same shell
`su [user] -c <command>` `command` is evaluated(expand) then passed | `command` executed as normal but with corresponding identity

- `sudo -l`: Check waht privileges are granted by `sudo`

#### chown - Change File Owner And Group
- `chown`: Superuser privileges required
- `chown [owner][:[group]] file...`
- `chown user file`: owner to `user`
- `chown :group file`: group owner to `group`
- `chown user:group file`: owner to `user` and group owner to `group`
- `chown user: file`: owner to `user` and group owner to default group of `user`

#### Exercising Our Privileges
- Refer to Textbook (P.105)
##### In Teaching Video
- `sudo adduser <username>`
- `sudo addgroup <groupname>`
- `sudo usermod -a -G <groupname> <username>

#### Changing Your Password
- `passwd [user]`: Default current user
- `man passwd`

## Processes
- `ps`, `top`, `jos `, `bg`, `fg`, `kill`, `killall`, `shutdown`
 
### How A Process Works
- When a system starts up, he kernel initiates a few of its own activities as processes and launches a program called **init** (PID 1). **init**, in turn, runs a series of shell scripts (located in `/etc`) called *init script*, which start all the system services. Many of these services are implemented as *daemon programs*, programs that just sit in the background and do their thing without having any user interface.
- A *parent process* launch one(s) *child process*(program)
- **PID** is assigned in ascending order

### Viewing Processes
- **TTY** is short for "Teletype", and refers to the *controlling terminal* for the process
- Time field of `ps` is the amount of CPU time consumed by the process
- `ps x`: Show all of out processes regardless of what terminal (if any) they are controlled by. (using this option, we see a list of every process that we own
- **STAT** field output by `ps x` means state of the process

State | Meaning
:---:|---
R | Running. This means that the process is running or ready to run.
S | Sleeping. The process is not running; rather, it is waiting for an event, such as a keystroke or network packet.
D | Uninterruptible Sleep. Process is waiting for I/O such as a disk drive.
T | Stopped. Process has been instructed to stop.
Z | A defunct or "zombie" process. This is a child process that has terminated, but has not been cleaned up by its parent.
< | A high priority process. It's possible to grant more importance to a process, giving it more time on the CPU. This property of a process is called *niceness*. A process with high priority is said to be less *nice* because its taking more of the CPU's time, which leaves less for everybody else.
N | A low priority process. A process with low priority (a "nice" process) will only get processor time after other processes with higher priority have been serviced.

- `ps aux`

BSD Style ps Column Header | Meaning
:---:|---
USER | User ID. This is the owner of the process.
%CPU | CPU usage in percent.
%MEM | Memory usage in percent.
VSZ | Virtual memory size.
RSS | Resident Set Size. The amount of physical memory (RAM) the process is using in kilobytes.
START | Time when the process started. For values over 24 hours, a date is used.

- `man ps`

> Qz:  
> `ps a` list all process run by tty (terminal) [\[reference\]](https://www.pslinux.online/ps-a.html)

#### Viewing Processes Dynamically With top
- Refer to Textbook (P.112)

### Controlling Processes
#### Interrupting A Process
- `CTRL-C`: *Interrupts* (terminate) current running process

#### Putting A Process In The Background
- `xlogo &`: Run xlogo in background; prompt message by *job control*: `[1] 28236` (job number, PID)
- job control
- `jobs`

#### Returning A Process To The Foreground
- `fg %1`: Bring background process to foregound `%1` is a *jobspec*

#### Stopping (Pausing) A Process
`CTRL-Z`: Stop the foreground process and put it to background
`bg %1`: Set `%1`(jobspec) to run in background.
> Note by Qz  
> By `xlogo &`, the program still runing (backgroundly)  
> By press `CTRL-Z`, terminal sent **TSTP** to the foreground program, the program then "paused" and seems like it's freezed

### Signals
- `kill PID` / `kill jobspce`
- `kill` sends a *signal* to a process; **TERM** by default
- `CTRL-C` sends a **INT**(Interrupt) signal
- `CTRL-Z` sends a **TSTP**(Terminal Stop) signal

#### Sending Signals To Processes With kill
- `kill [-signal] PID...`: Sends **TERM**(Terminate) if `[-signal]` is not specified; `[-signal]` can be represented as signal number or signal name in table below (or signal name prefixed with "SIG")
- Example: `kill -1 13601`, `kill -HUP 13601`, `kill -SIGHUB 13601`. All commands send a **HUP** to 13601(PID of the process)
- Instead of PID, we can specify a jobspec

Signal Number | Name | Meaning
:---:|:---:|---
1 | HUP | Hangup. This is a vestige of the good old days when terminals were attached to remote computers with phone lines and modems. The signal is used to indicate to programs that the controlling terminal has “hung up.” The effect of this signal can be demonstrated by closing a terminal session. The foreground program running on the terminal will be sent the signal and will terminate. This signal is also used by many daemon programs to cause a reinitialization. This means that when a daemon is sent this signal, it will restart and re-read its configuration file. The Apache web server is an example of a daemon that uses the **HUP** signal in this way.
2 | INT | Interrupt. Performs the same function as the `Ctrl-C` key sent from the terminal. It will usually terminate a program.
9 | KILL | Kill. This signal is special. Whereas programs may choose to handle signals sent to them in different ways, including ignoring them all together, the **KILL** signal is never actually sent to the target program. Rather, the kernel immediately terminates the process. When a process is terminated in this manner, it is given no opportunity to “clean up” after itself or save its work. For this reason, the KILL signal should only be used as a last resort when other termination signals fail.
15 | TERM | Terminate. This is the default signal sent by the **kill** command. If a program is still “alive” enough to receive signals, it will terminate.
18 | CONT | Continue. This will restore a process after a **STOP** signal.
19 | STOP | Stop. This signal causes a process to pause without terminating. Like the **KILL** signal, it is not sent to the target process, and thus it cannot be ignored.

- Processes, like files, have owners, and you must be the owner of a process (or the superuser) in order to send it signals with **kill**.

Number | Name | Meaning
:---:|:---:|---
3 | QUIT | Quit.
11 | SEGV | Segmentation Violation. This signal is sent if a program makes illegal use of memory, that is, it tried to write somewhere it was not allowed to.
20 | TSTP | Terminal Stop. This is the signal sent by the terminal when the `Ctrl-z` key is pressed. Unlike the **STOP** signal, the **TSTP** signal is received by the program but the program may choose to ignore it.
28 | WINCH | Window Change. This is the signal sent by the system when a window changes size. Some programs, like **top** and **less** will respond to this signal by redrawing themselves to fit the new window dimensions.

- `kill -l`: List all signals

#### Sending Signals To Multiple Processes With killall
- `killall [-u user] [-signal] name...`

### Shutting Down The System
- `halt`, `poweroff`, `reboot`, `shutdown`
- `sudo shutdown -h now`, `sudo shutdown -r now`
- Once the **shutdown** command is executed, a message is "broadcast" to all logged-in user warning them of the impending event.

### More Process Related Commands
Command | Description
:---:|---
pstree | Outputs a process list arranged in a tree-like pattern showing the parent/child relationships between processes.
vmstat | Outputs a snapshot of system resource usage including, memory, swap and disk I/O. To see a continuous display, follow the command with a time delay (in seconds) for updates. for example: `vmstat 5`. Terminate the output with `CTRL-C`.
xload | A graphical program that draws graph showing system load over time.
tload | Similar to the **xload** program, but draws the graph in the terminal. Terminate the output with `CTRL-C`.

## The Environment
- `printenv`, `set`, `export`, `alias`

### What Is Stored In The Environment?
- *environment variables*, *shell variables*

#### Examining The Environment
- `printenv`: Print environment variables
- `set`: Print shell variable environment variable and defined shell function (sorted by alphabetical order)
- `printenv var_name`: Example: `printenv USER`
- Neither `set` nor `printenv` displays aliases

#### Some Interesting Variables
Environment Variable | Contents
:---:|---
DISPLAY | The name of your display if you are running a graphical environment. Usually this is “:0”, meaning the first display generated by the X server.
EDITOR | The name of the program to be used for text editing.
SHELL | The name of your shell program.
HOME | The pathname of your home directory.
LANG | Defines the character set and collation order of your language.
OLD_PWD | The previous working directory.
PAGER | The name of the program to be used for paging output. This is often set to `/usr/bin/less`.
PATH | A colon-separated list of directories that are searched when you enter the name of a executable program.
PS1 | Prompt String 1. This defines the contents of the shell prompt. As we will later see, this can be extensively customized.
PWD | The current working directory.
TERM | The name of your terminal type. Unix-like systems support many terminal protocols; this variable sets the protocol to be used with your terminal emulator.
TZ | Specifies your timezone. Most Unix-like systems maintain the computer’s internal clock in Coordinated Universal Time (UTC) and then displays the local time by applying an offset specified by this variable.
USER | Your username.

### How Is The Environment Established?
When we log on to the system, the **bash** program starts, and reads a series of configuration scripts called *startup files*,
which define the default environment shared by all users. This is followed by more startup files in our home directory that
define our personal environment. The exact sequence depends on the type of shell session being started. There are two kinds:
a login shell session and a non-login shell session.

Startup Files For Login Shell Session | Contents
---|---
`/etc/profile` | A global configuration script that applies to all users.
`~/.bash_profile` | A user's personal startup file. Can be used to extend or override settings in the global configuration script.
`~/.bash_login` | If `~/.bash_profile` is not found, bash attempts to read this script.
`~/.profile` | If neither `~/.bash_profile` nor `~/.bash_login` is found, bash attempts to read this file. This is the default in Debian-based distributions, such as Ubuntu.


Startup Files For Non-login Shell Sessions | Contents
---|---
`/etc/bash.bashrc` | A global configuration script that applies to all users.
`~/.bashrc` | A user's personal startup file. Can be used to extend or override settings in the global configuration script.

- In addition to reading the startup files above, non-login shells also inherit the environment from their parent process, usually a login shell.

#### What's In A Startup File?
- Refer to Textbook (P.130)
- Commands(programs) are searched in the list of directories defined in **PATH** variable
- **PATH** variable is often set by the `/etc/profile` startup file and with this code: `PATH=$PATH:$HOME/bin`, where `:` is used to concatenate strings

### Modifying The Environment
#### Which Files Should We Modify?
- As a general rule, to add directories to your **PATH**, or define additional environment variables, place those changes in `~/.bash_profile` (or equivalent, accroding to your distribution. For example, Ubuntu uses `~/.profile`). For everything else, place the changes in `~/.bashrc`.

#### Text Editors
- Graphical based: gedit, kedit, kwrite, kate
- Text based: nano, vi(vim), emacs

#### Using A Text Editor
> Qz: I use vim. Not going to cover operations in nano or else  
> Please refer to Textbook (P.133)
- `gedit filename`
- `nano filename`
- `^X`: Common notation of "CTRL-X"

#### Activating Our Changes
- `source .bashrc`

## A Gentle Introduction To vi
> Qz: skip  
> Please refer to Textbook

## Customizing The Prompt  TODO
### Anatomy Of A Prompt
Escape Code | Value Displayed
:---:|---
\\a | ASCII bell. This makes the computer beep when it is encountered.
\\d | Current date in day, month, date format. For example, "Mon May 26".
\\h | Hostname of local machine minus the trailing domain name.
\\H | Full hostname

## Storage Media
- `mount`, `umount`, `fsck`, `fdisk`, `mkfs`, `fdformat`, `dd`, `genisoimage(mkisofs)`, `wodim`, `md5sum`

### Mounting And Unmounting Storage Devices
- `/etc/fstab`: example content: `LABEL=/home  /home   ext4  defaults  1 2`

Field of `/etc/fstab` | Content | Description | Example
:---:|:---:|---|:---:
1 | Device | Traditionally, this field contains the actual name of a device file associated with the physical device, such as `/dev/sda1` (the first partition of the first detected hard disk). But with today's computers, which have many devices that are hot pluggable (like USB drives), many modern Linux distributions associate a device with a text label instead. This label (which is added to the storage media when it is formatted) can be either a simple text label, or a randomly generated UUID (Universally Unique IDentifier). This label is read by the operating system when the device is attached to the system. That way, no matter which device file is assigned to the actual physical device, it can still be correctly identified. | `LABEL=/home`
2 | Mount Point | The directory where the device is attached to the file system tree. | `/home`
3 | File System Type | Linux allows many file system types to be mounted. Most native Linux file systems are Fourth Extended File System (ex4), but many others are supported, such as FAT16(msdos), FAT32(vfat), NTFS(ntfs), CD-ROM(iso9660), etc | `ext4`
4 | Options | File systems can be mounted with various options. It is possible, for example, to mount file systems as read-only, or to prevent any programs from being executed from them (a useful security feature for removable media.) | `default`
5 | Frequency | A single number that specifies if and when the file system is to be backed up with the `dump` command. | `1`
6 | Order | A single number that spacifies in what order file system should be checked with the `fsck` command.

#### Viewing A List of Mounted File Systems
- Example of an entry of `mount`: `/dev/sda2 on / type ext4 (rw)`
- Format of an entry of `mount`: *device* on *mount_point* type *file_system_type* (*options*)

<table>
  <tr>
    <th colspan="6">Example of an entry of <code>mount</code></th>
  </tr>
  <tr>
    <td>/dev/sda2</td>
    <td>on</td>
    <td>/</td>
    <td>type</td>
    <td>ext4</td>
    <td>(rw)</td>
  </tr>
  <tr>
    <td><i>device</i></td>
    <td>on</td>
    <td><i>mount_point</i></td>
    <td>type</td>
    <td><i>file_system_type</i></td>
    <td>(<i>options</i>)</td>
  </tr>
</table>

- Steps to `mount` and `umount`
    1. Plugin your device, says your insert a CD-ROM
    2. Find your device in system tree, says it is on `/dev/sr0`
    3. If it is automatically mounted, `mount` command show where it's mounted.
    4. If the device isn't automatically mounted.
        1. Make a directory for your device, we're going to mount it on the directory. Says new directory is `/mnt/cdrom`
            - If mount on existing directory, the content inside will be unvisible until umount.
        2. Mount your CD-ROM by `mount -t iso9660 /dev/sr0 /mnt/cdrom`
            - The command is in format: mount -t *file-system-type* *path-to-device* *mount-point*
    5. To umount your CD-ROM, `umount /dev/sr0` or `umount /mnt/cdrom`
        - If you were inside the mount point (cd /mnt/cdrom), umount process will not be success (Device is busy).
    6. Your system may automatically mount back the device you umounted

#### Determining Device Names
Device | Storage Device Names
:---:|---
/dev/fd\* | Floppy disk drives.
/dev/hd\* | IDE (PATA) disks on older systems. Typical motherboards contain two IDE connectors or channels, each with a cable with two attachment points for drives. The first drive on the cable is called the master device and the second is called the slave device. The device names are ordered such that /dev/hda refers to the master device on the first channel, /dev/hdb is the slave device on the first channel; /dev/hdc, the master device on the second channel, and so on. A trailing digit indicates the partition number on the device. For example, /dev/hda1 refers to the first partition on the first hard drive on the system while /dev/hda refers to the entire drive.
/dev/lp\* | Printers.
/dev/sd\* | SCSI disks. On modern Linux systems, the kernel treats all disk- like devices (including PATA/SATA hard disks, flash drives, and USB mass storage devices such as portable music players, and digital cameras) as SCSI disks. The rest of the naming system is similar to the older /dev/hd\* naming scheme described above.
/dev/sr\* | Optical drives (CD/DVD readers and burners).

#### Manipulating Partitions With fdisk
- `fdisk`
- `mkfs`: Make file system. Syntax: mkfs -t *filesystem_type* *device*; Example: `mkfs -t ext4 /dev/sda`

#### Creating A New File System With mkfs
- `fsck`: File Sytem ChecK. Umount a device first, then `fsck /dev/sr0`

#### Moving Data Directly To/From Devices
- `dd`: Data Definition
- Syntax of `dd`: dd if=*input_file* of=*output_file* [bs=*block_size* [count=*blocks*]]
- `dd if=/dev/sda3 of=/dev/sda4`
- `dd if=/dev/sda3 of=disk.img`

#### Creating An Image Copy Of A CD-ROM
- Say we had an Ubuntu CD and in device `/dev/cdrom` To make a copy: `dd if=/dev/cdrom of=ubuntu.iso`

#### Creating An Image From A Collection Of Files
1. Create a directory containing all files we wish to include in the image, says the dirctory is `~/toCD`
2. `genisoimage -o cd-rom.iso -R -J ~/toCD`; `-R`: adds metadata for the *Rock Ridge extensions*, which allows the use of long filenames and POSIX style file permissions. `-J` enables the *Joliet extensions*, which permit long filename for Windows.

#### Mounting An ISO Image Directly
- Mount .iso file as it were already on optical media: `mount -t iso9660 -o loop image.iso /mnt/iso_image`

#### Blanking A Re-Writable CD-ROM
- Rewritable CD-RW media needs to be erased or *blanked* before it can be reused.
- Blanking a CD, syntax: wodim dev=*dev_name* blank=*type*
- `wodim dev=/dev/cdrw blank=fast`: blank device `/dev/cdrw`. `wodim` offers several types. The most minimal (and fast) is the "fast" type

#### Writing An Image
- `wodim dev=/dev/cdrw image.iso`

#### Extra Credit
`md5sum image.iso`

## Networking
- `ping`, `traceroute`, `ip`, `netstat`, `ftp`, `wget`, `ssh`

#### ping
- `ping domain.name`, `ping ip.number`

#### ip
```
Output of ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp7s0f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN group default qlen 1000
    link/ether f0:76:1c:29:6a:43 brd ff:ff:ff:ff:ff:ff
3: wlp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 30:10:b3:80:bf:7e brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.6/24 brd 192.168.0.255 scope global dynamic wlp8s0
       valid_lft 5147sec preferred_lft 5147sec
    inet6 fe80::2a0:9fa9:7ef7:322b/64 scope link 
       valid_lft forever preferred_lft forever
```
- `lo`: Loopback interface, a virtual interface that system uses to "talk to itself"
- `eth0`: Ethernet interface
- `UP`: Indicate interface is enabled.

#### netstat
- `netstat -ie`: `-i`: (--interfaces) Display a table of all network interface; `-e`: (--extend) Display additional information.
- `netstat -r`: Kernel's network routing table; `-r`: (--route) same as route -e
    ```
    Output of netstat -r
    Kernel IP routing table
    Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
    default         192.168.42.129  0.0.0.0         UG        0 0          0 enp0s20u1
    link-local      *               255.255.0.0     U         0 0          0 enp0s20u1
    192.168.42.0    *               255.255.255.0   U         0 0          0 enp0s20u1
    ```
    - `192.168.42.0`: Destination IP address end with a zero refer to network rather than individual host, means any host on the LAN.
    - `Gateway`: The gateway for current IP address to the `Destination`; `*` means no needs for a gateway.
    - `default`: destinations for addresses not list in otherwise listed `Destination`

#### ftp
- ftp: File Transfer Protocol
- Transfering of ftp is not encrypted.
- Example and explanation please Refer to Textbook.

#### wget
- Useful for downloading content both web and FTP sites.
- `wget http://linuxcommand.org/index.php`

### Secure Communicatin with Remote Hosts
- `rlogin`, `telnet`: Programs used to log in to remote hosts. Transimiting messages are in cleartext.

#### ssh
- Secure Shell
- SSH soloves the two basic problems of secure communication with a remote host. First, it authenticates that the remote host is who it says it is (thus preventing so-called "man in the middle" attacks), and second, it encrypts all of the communications between the local and remote hosts.
- SSH server listen port 22 for incoming connections.
- OpenSSH: An implementation of SSH.
- `ssh hostname`
- `ssh username@hostname`
- `ssh hostname command`: Execute a command on ssh host and display the result on localhost
- OpenSSH package includes two programs that can make use of SSH-encrypted tunnel to copy file across the network.
    - scp (secure copy)
        - `scp hostname:document`: Copy `document` from `hostname`
        - `scp username@hostname:document`
    - sftp: Implement ftp functionality on SSH server
> Tip: The SFTP protocol is supported by many of the graphical file managers found in Linux distributions. Using either Nautilus (GNOME) or Konqueror (KDE), we can enter a URI beginning with **sftp://** into the location bar and operate on files stored on remote system running an SSH server.

## Searching For Files
